**Lambda em NodeJS**

Aplicação serveless criada com o framework serveless.

https://www.serverless.com/open-source/

Requisitos:

    AWS account
    Node.js
    AWS CLI and configure it

Para instalar o framework serveless:

`npm install serverless -g`

Para criar um projeto serveless:

`serverless create --template aws-nodejs --path candidate-service --name candidate`

Para deployar o projeto:

`sls deploy`