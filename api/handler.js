'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk'); 

AWS.config.setPromisesDependency(require('bluebird'));

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.clientes = (event, context, callback) => {
  var nome = ""
  if (typeof event.queryStringParameters != 'undefined') { 
    nome = event.queryStringParameters.nome
  }
  console.log("nome ", nome)
  var params = {
      TableName: process.env.TABLE_NAME,
      FilterExpression: "contains(#nome,:nome)",
      ExpressionAttributeNames:{
          "#nome": "nome"
      },
      ExpressionAttributeValues: {
        ":nome": nome 
      }  
  
  };

  console.log("Lendo tabela.");
  const onScan = (err, data) => {
      if (err) {
          console.log('Erro ao ler tabela. Error JSON:', JSON.stringify(err, null, 2));
          callback(err);
      } else {
          console.log("Leitura efetuada com sucesso.");
          return callback(null, {
              statusCode: 200,
              body: JSON.stringify({
                  clientes: data.Items
              })
          });
      }
  };

  dynamoDb.scan(params, onScan);
};

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };

